#!/usr/bin/env python3
"""
Python OpenGL practical application.
"""
# Python built-in modules
import os                          
from itertools import cycle

import OpenGL.GL as GL              
import glfw                         
import numpy as np                 
import pyassimp                   
import pyassimp.errors          
import bisect
import random
import time   

from transform  import translate, rotate, scale, vec, normalized, lerp
from transform import frustum, perspective
from transform import Trackball, identity
from transform import quaternion_slerp, quaternion_matrix, quaternion, quaternion_from_euler

from PIL import Image               # load images for textures


# ------------ low level OpenGL object wrappers ----------------------------
class Shader:
    """ Class to create and automatically destroy shader program """
    @staticmethod
    def _compile_shader(src, shader_type):
        src = open(src, 'r').read() if os.path.exists(src) else src
        src = src.decode('ascii') if isinstance(src, bytes) else src
        shader = GL.glCreateShader(shader_type)
        GL.glShaderSource(shader, src)
        GL.glCompileShader(shader)
        status = GL.glGetShaderiv(shader, GL.GL_COMPILE_STATUS)
        src = ('%3d: %s' % (i+1, l) for i, l in enumerate(src.splitlines()))
        if not status:
            log = GL.glGetShaderInfoLog(shader).decode('ascii')
            GL.glDeleteShader(shader)
            src = '\n'.join(src)
            print('Compile failed for %s\n%s\n%s' % (shader_type, log, src))
            return None
        return shader

    def __init__(self, vertex_source, fragment_source):
        """ Shader can be initialized with raw strings or source file names """
        self.glid = None
        vert = self._compile_shader(vertex_source, GL.GL_VERTEX_SHADER)
        frag = self._compile_shader(fragment_source, GL.GL_FRAGMENT_SHADER)
        if vert and frag:
            self.glid = GL.glCreateProgram() 
            GL.glAttachShader(self.glid, vert)
            GL.glAttachShader(self.glid, frag)
            GL.glLinkProgram(self.glid)
            GL.glDeleteShader(vert)
            GL.glDeleteShader(frag)
            status = GL.glGetProgramiv(self.glid, GL.GL_LINK_STATUS)
            if not status:
                print(GL.glGetProgramInfoLog(self.glid).decode('ascii'))
                GL.glDeleteProgram(self.glid)
                self.glid = None

    def __del__(self):
        GL.glUseProgram(0)
        if self.glid:                      # if this is a valid shader object
            GL.glDeleteProgram(self.glid)  # object dies => destroy GL object


class VertexArray:
    """Class to create and self destroy vertex array objects"""
    def __init__(self, attributes, index=None, usage=GL.GL_STATIC_DRAW):

        # create vertex array object, bind it
        self.glid = GL.glGenVertexArrays(1)
        GL.glBindVertexArray(self.glid)
        self.buffers = []  # we will store buffers in a list
        nb_primitives, size = 0, 0

        # load a buffer per initialized vertex attribute (=dictionary)
        for loc, data in enumerate(attributes):
            if data is None:
                continue

            # bind a new vbo, upload its data to GPU, declare its size and type
            self.buffers += [GL.glGenBuffers(1)]
            data = np.array(data, np.float32, copy=False)
            nb_primitives, size = data.shape
            GL.glEnableVertexAttribArray(loc)  # enable a generic vertex attribute array
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.buffers[-1])
            GL.glBufferData(GL.GL_ARRAY_BUFFER, data, usage)
            GL.glVertexAttribPointer(loc, size, GL.GL_FLOAT, False, 0, None)

        # optionally create and upload an index buffer for this object
        self.draw_command = GL.glDrawArrays
        self.arguments = (0, nb_primitives)
        if index is not None:
            self.buffers += [GL.glGenBuffers(1)]
            index_buffer = np.array(index, np.int32, copy=False)
            GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, self.buffers[-1])
            GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, index_buffer, usage)
            self.draw_command = GL.glDrawElements
            self.arguments = (index_buffer.size, GL.GL_UNSIGNED_INT, None)

        # cleanup and unbind
        GL.glBindVertexArray(0)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, 0)
        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, 0)

    def draw(self, primitive):
        """draw a vertex array, either as direct array or indexed array"""
        GL.glBindVertexArray(self.glid)
        self.draw_command(primitive, *self.arguments)
        GL.glBindVertexArray(0)

    def __del__(self): 
        GL.glDeleteVertexArrays(1, [self.glid])
        GL.glDeleteBuffers(len(self.buffers), self.buffers)



class Texture:
    """ Class to create and automatically destroy textures """
    def __init__(self, file, wrap_mode=GL.GL_REPEAT, min_filter=GL.GL_LINEAR,
                 mag_filter=GL.GL_LINEAR_MIPMAP_LINEAR):
        self.glid = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.glid)
        # helper array stores texture format for every pixel size 1..4
        format = [GL.GL_LUMINANCE, GL.GL_LUMINANCE_ALPHA, GL.GL_RGB, GL.GL_RGBA]
        try:
            # imports image as a numpy array in exactly right format
            tex = np.array(Image.open(file))
            format = format[0 if len(tex.shape) == 2 else tex.shape[2] - 1]
            GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, tex.shape[1],
                            tex.shape[0], 0, format, GL.GL_UNSIGNED_BYTE, tex)

            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, min_filter)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, mag_filter)
            GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
            message = 'Loaded texture %s\t(%s, %s, %s, %s)'
            print(message % (file, tex.shape, wrap_mode, min_filter, mag_filter))
        except FileNotFoundError:
            print("ERROR: unable to load texture file %s" % file)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

    def __del__(self):
        GL.glDeleteTextures(self.glid)
        
        
class MultiTexture:
    """ Class to create and automatically destroy textures """
    def __init__(self, file, wrap_mode=[GL.GL_LINEAR,GL.GL_REPEAT], min_filter=[GL.GL_LINEAR],
                 mag_filter=[GL.GL_LINEAR_MIPMAP_LINEAR]):
        self.file=file
        self.glid = GL.glGenTextures(len(file))
        for i in range(len(file)):
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.glid[i])
            # helper array stores texture format for every pixel size 1..4
            format = [GL.GL_LUMINANCE, GL.GL_LUMINANCE_ALPHA, GL.GL_RGB, GL.GL_RGBA]
            try:
                # imports image as a numpy array in exactly right format
                tex = np.array(Image.open(file[i]))
                format = format[0 if len(tex.shape) == 2 else tex.shape[2] - 1]
                GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, tex.shape[1],
                               tex.shape[0], 0, format, GL.GL_UNSIGNED_BYTE, tex)
                
                print(wrap_mode[i])
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, wrap_mode[i])
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, wrap_mode[i])
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, min_filter[i])
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, mag_filter[i])
                GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
                message = 'Loaded texture %s\t(%s, %s, %s, %s)'
                print(message % (file[i], tex.shape, wrap_mode[i], min_filter[i], mag_filter[i]))
            except FileNotFoundError:
                print("ERROR: unable to load texture file %s" % file[i])
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

    def __del__(self):
        GL.glDeleteTextures(len(self.file),self.glid)
        
        
        
class  CubeTexture:
    """ Helper class to create and automatically destroy skybox texture """
    def __init__(self, file, wrap_mode= GL.GL_CLAMP_TO_EDGE, min_filter=GL.GL_LINEAR,
                 mag_filter=GL.GL_LINEAR):
        self.glid = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, self.glid)
        # helper array stores texture format for every pixel size 1..4
        for i in range(len(file)):
            try:
                # imports image as a numpy array in exactly right format
                tex = np.array(Image.open(file[i]))
                GL.glTexImage2D(GL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL.GL_RGB, tex.shape[1],
                            tex.shape[0], 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, tex)
                GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_S, wrap_mode)
                GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_T, wrap_mode)
                GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_WRAP_R, GL.GL_CLAMP_TO_EDGE)
                GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MAG_FILTER, min_filter)
                GL.glTexParameteri(GL.GL_TEXTURE_CUBE_MAP, GL.GL_TEXTURE_MIN_FILTER, mag_filter)
                message = 'Loaded texture %s\t(%s, %s, %s, %s)'
                print(message % (file[i], tex.shape, wrap_mode, min_filter, mag_filter))
            except FileNotFoundError:
                print("ERROR: unable to load texture file %s" % file[i])
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0)

    def __del__(self):  # delete GL texture from GPU when object dies
        GL.glDeleteTextures(self.glid)

# -------------- shaders for physical animation ---------------------------------------------------

PART_VERT = """#version 330 core
layout(location = 0) in vec3 in_offset;
layout(location = 1) in vec3 in_start_pos;
layout(location = 2) in vec3 in_start_vel;
layout(location = 3) in float in_offset_start_time;
layout(location = 4) in float in_size;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;
uniform int type;
uniform float global_time;
uniform float particle_life_time;
uniform int showmode;

out float l_time;
out vec2 fragTexCoord;
out vec3 centerp;



void main()
{  
   float t = mod( global_time+in_offset_start_time, particle_life_time);
   vec3 pos_view=vec3(0);
   if (type==1) {
       float g=9.8;
       //slow down time for the scene
       t=t/5;
       vec3 center = in_start_pos + in_start_vel * t + vec3(0, 0, -g) * t * t / 2;
       center=vec3(view* model*vec4(center,1));
       pos_view = vec3(center.x+ in_offset.x*t*in_size,center.y+ in_offset.y*t*in_size, center.z);
       gl_Position=projection*vec4(pos_view,1);
       fragTexCoord = vec2(0.5) + vec2(in_offset* 0.5);
     }
   if (type==2){    
       vec3 center =in_start_pos+ in_start_vel*t;
       pos_view = vec3(center.x+ in_offset.x*in_size,center.y+ in_offset.y*in_size, center.z);
       gl_Position=vec4(pos_view,1);
       fragTexCoord = vec2(0.5) + vec2(in_offset);
     }
    centerp=pos_view;
    l_time=in_offset_start_time;


}"""

PART_FRAG = """#version 330 core
uniform sampler2D diffuseMap;
uniform int type;
uniform float global_time;
uniform float particle_life_time;
uniform int showmode;

in vec2 fragTexCoord;
in vec3 centerp;
in float l_time;

out vec4 outColor;

void main() {
   float s=global_time;
   float t = mod(global_time+l_time, particle_life_time);
   float k=t/particle_life_time;
   float a=1;
   if (k>0.6){
     a=1-(k-0.6)/0.4;
   }
   if (k<0.4){
     a=k/0.4;
   }
   vec4 FragColor = texture(diffuseMap, fragTexCoord)*a; 
   if ((type==2)&&(showmode==0)){ 
    discard;
   }
   outColor=FragColor;     

   
}"""

       
 # -------------- shaders for skinning dino for current scene --------------------------      
MAX_VERTEX_BONES = 4
MAX_BONES = 128

SKINNING_VERT = """#version 330 core
uniform mat4 projection, view;
const int MAX_VERTEX_BONES=%d, MAX_BONES=%d;
uniform mat4 boneMatrix[MAX_BONES];

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec4 bone_ids;
layout(location = 3) in vec4 bone_weights;


out vec3 fragPos;
out vec4 vColor;
out vec3 n;
out vec4 vPosition;

void main() {
    int b_ix=int(bone_ids.x);
    int b_iy=int(bone_ids.y);
    int b_iz=int(bone_ids.z);
    int b_iw=int(bone_ids.w);
    mat4 skinMatrix = boneMatrix[b_ix]*bone_weights.x + boneMatrix[b_iy]*bone_weights.y+boneMatrix[b_iz]*bone_weights.z +boneMatrix[b_iw]*bone_weights.w;
    
    vec4 wPosition4 = skinMatrix * vec4(position, 1.0);
    gl_Position = projection * view * wPosition4;
     
    fragPos=vec3(view*wPosition4);
    n=normalize(transpose(inverse(mat3(view*skinMatrix)))*color);
    vPosition=gl_Position;
    vColor=vec4(color, 1);

}
""" % (MAX_VERTEX_BONES, MAX_BONES)  

        
# ------------  vertex and fragment shaders with illumination ------------------------
COLOR_VERT = """#version 330 core
uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 vertcolor;
layout(location = 2) in vec2 uv_coord;

out vec2 fragTexCoord;
out vec3 fragPos;
out vec4 color;
out vec3 n;
out vec4 vPosition;


void main() {
    gl_Position = projection*view*model*vec4(position, 1);
    vPosition=gl_Position;
    n=normalize(transpose(inverse(mat3(view*model)))*vertcolor);
    color=vec4(vertcolor, 1);  //for normals and mode 1
    fragPos=vec3(view* model *vec4(position, 1));
    fragTexCoord = uv_coord;    // texture uv coordinates

}"""

COLOR_FRAG = """#version 330 core
uniform vec3 K_d;
uniform vec3 K_a;
uniform vec3 K_s;
uniform float s;
uniform float time;
uniform int mode;
uniform sampler2D diffuseMap;
uniform sampler2D diffuseMap1;
uniform sampler2D diffuseMap2;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

in vec2 fragTexCoord;
in vec4 color;
in vec3 fragPos;
in vec3 n;
in vec4 vPosition;
out vec4 outColor;

vec3 CalcPointLight(vec3 light, vec3 viewvec)
{
    vec3 lightDir = normalize(light-fragPos);
    vec3 reflectlight=reflect(-lightDir,n);
    float diffuse=max(dot(n,lightDir),0.0);
    float specular=0.0;
    if (diffuse>0.0){specular= max(pow(dot(reflectlight, viewvec), s), 0.0);}
    float distance=length(light-fragPos);
    float att = 1.0/(0.0075*distance * distance+0.045*distance+1.0);    
    vec3 ambientp  = K_a;
    vec3 diffusep  = K_d*diffuse;
    vec3 specularp = K_s*specular;
    return ((ambientp+diffusep+specularp)*att);
}    

vec3 CalcDirLight(vec3 light, vec3 viewvec)
{
    vec3 lightDir = normalize(-light);
    vec3 reflectlight=reflect(-lightDir,n);
    float diffuse=max(dot(n,lightDir),0.0);
    float specular=0.0;
    if (diffuse>0.0){specular= max(pow(dot(reflectlight, viewvec), s), 0.0);} 
    vec3 ambientp  = K_a;
    vec3 diffusep  = K_d*diffuse;
    vec3 specularp = K_s*specular;
    return (ambientp+diffusep+specularp);
}

void main()
{
    vec3 result;
    vec3 viewvec=normalize(-fragPos);
    vec3 dir1=vec3(1,1,-1);
    vec3 dir2=vec3(-1,-1,-1);
    vec3 point1=vec3(view*vec4(vec3(80,-57,30), 1));
    vec3 point2=vec3(view*vec4(vec3(96,-72,30), 1));
    result = CalcDirLight(dir1, viewvec)+CalcDirLight(dir2, viewvec)+CalcPointLight(point1, viewvec)+CalcPointLight(point2, viewvec);
    if (mode==0) outColor=color;
    if (mode==1) outColor=vec4(result,1); //  without texture
    if (mode==2) outColor=texture(diffuseMap, fragTexCoord)*vec4(result,1);
    if (mode==3){
        outColor=texture(diffuseMap1, fragTexCoord)*vec4(result,1);
        vec4 vPosition_temp=mat4(inverse(projection*view))*vPosition;
        vPosition_temp=vPosition_temp/vPosition_temp.w;
        vec4 detcolor=texture(diffuseMap2, 0.1*vPosition_temp.xy);
        outColor=outColor*detcolor*1.8;
    }       
    
}"""  


# ------------  vertex and fragment shaders for physical based rendering ------------------------
PBR_VERT = """#version 330 core
uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 vertcolor;

out vec3 fragPos;
out vec3 n;


void main() {
    gl_Position = projection*view*model*vec4(position, 1);
    n=normalize(transpose(inverse(mat3(view*model)))*vertcolor);
    fragPos=vec3(view* model *vec4(position, 1));

}"""

PBR_FRAG = """#version 330 core
uniform vec3 albedo;
uniform vec3 f0;
uniform float roughness;
uniform float time;

in vec3 fragPos;
in vec3 n;
out vec4 outColor;

//microfacet distribution
float Distribution(float cosTheta, float alpha) {
    float alphasqr = alpha * alpha;
    float sqr = clamp(cosTheta * cosTheta,0,1);
    float den = sqr * alphasqr + (1.0 - sqr);
    return alphasqr/(3.14 * den * den);
}   

//Smith visibility function
float Smith(float cosTheta, float alpha) {
    float cosTheta_sqr = clamp(cosTheta*cosTheta,0,1);
    float tan2 = ( 1 - cosTheta_sqr ) / cosTheta_sqr;
    float s = 2 / ( 1 + sqrt( 1 + alpha * alpha * tan2 ) );
    return s;
}

vec3 FresnelSchlick(vec3 F0, float cosTheta) {
    return F0 + (1.0 - F0) * pow(1.0 - clamp(cosTheta,0,1), 5.0);
}

vec3 CookTorrance(vec3 light, vec3 viewvec, vec3 a, vec3 f, float rough) {

    vec3 lightDir = normalize(-light);
    vec3 h = normalize(viewvec+lightDir);
    float nl = dot(n, lightDir);
    if (nl <= 0.0) return vec3(0,0,0);
    float nv = dot(n, viewvec);
    if (nv <= 0.0) return vec3(0,0,0);
    float nh = dot(n, h);
    float hv = dot(h, viewvec);    
    float roughnsqr = rough*rough;

    float G = Smith(nv, roughnsqr) * Smith(nl, roughnsqr);
    float D = Distribution(nh, roughnsqr);
    vec3 F = FresnelSchlick(f, hv);


    vec3 specK = G*D*F*0.25/(nv+0.001);    
    vec3 diffK = clamp(1.0-F,0,1);
    return a*diffK*nl/3.14 + specK;
}   

void main()
{
    vec3 result;
    vec3 viewvec=normalize(-fragPos);
    vec3 dir1=vec3(1,1,-1);
    vec3 dir2=vec3(-1,-1,-1);
    result = CookTorrance(dir1, viewvec, albedo, f0, roughness)+CookTorrance(dir2, viewvec, albedo, f0, roughness);
    outColor=vec4(result,1); //  without texture
}""" 

# -------------- shaders for skybox ---------------------------------------------------
SKY_VERT="""#version 330 core
layout (location = 0) in vec3 aPos;
out vec3 TexCoords;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
void main()
{
    TexCoords = aPos;
    vec4 pos = projection * mat4(mat3(view)) * vec4(aPos, 1.0);
    gl_Position = pos.xyww;
         
}"""      
        
SKY_FRAG="""#version 330 core
out vec4 FragColor;
in vec3 TexCoords;
uniform samplerCube skybox;
void main()
{    
 
   FragColor = texture(skybox,TexCoords);
       
}"""    

# -------------- shaders for planet ---------------------------------------------------

PLANET_VERT="""#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 normals;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 TexCoords;
out vec3 vPos;
out vec3 norm;

void main()
{
    TexCoords = aPos;
    norm=normalize(transpose(inverse(mat3(view*model)))*normals);  
    vec4 pos = projection*view*model*vec4(aPos, 1.0);
    gl_Position = pos;
    
    vPos=vec3(view*model*vec4(aPos, 1.0));
    
         
}"""      
        
PLANET_FRAG="""#version 330 core

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform samplerCube skybox;

out vec4 outColor;
in vec3 TexCoords;
in vec3 norm;
in vec3 vPos;

void main()
{
   vec3 I= normalize(vPos);
   vec3 Refl=reflect(I,normalize(norm));
   Refl=mat3(inverse(view))*Refl;
   vec3 FragColor_refl = vec3(texture(skybox, Refl));
   float eta=1/1.309;

   float F=((1.0-eta)*(1.0-eta))/((1.0+eta)*(1.0+eta));

   vec3 Refr= refract(I, normalize(norm), eta);
   Refr=mat3(inverse(view))*Refr;

   float iv = dot(normalize(norm), -I);
   float Ratio=F + (1.0 - F) * pow(1.0 - clamp(iv,0,1), 5.0);
   vec3 FragColor_refr = vec3(texture(skybox, Refr));

   vec3 FragColor=mix(FragColor_refr,FragColor_refl,Ratio);


   outColor=vec4(FragColor,1);



}"""



# -------------- classes for hierarchy -----------------------------------------
class Node:
    """ Scene graph transform and parameter broadcast node """
    def __init__(self, name='', children=(), transform=identity(), **param):
        self.transform, self.param, self.name = transform, param, name
        self.children = list(iter(children))

    def add(self, *drawables):
        self.children.extend(drawables)

    def draw(self, projection, view, model, **param):
        param = dict(param, **self.param)
        model = model @ self.transform  
        for child in self.children:
            child.draw(projection, view, model, **param)
  
class RotationControlNode(Node):
    def __init__(self, key_up, key_down, axis, angle=0,translate=identity(),scale=identity(), **param):
        super().__init__( **param)
        self.angle, self.axis = angle, axis
        self.key_up, self.key_down = key_up, key_down
        self.local_translate=translate
        self.local_scale=scale

    def draw(self, projection, view, model, win=None, **param):
        assert win is not None
        self.angle -= 2 * int(glfw.get_key(win, self.key_up) == glfw.PRESS)
        self.angle += 2 * int(glfw.get_key(win, self.key_down) == glfw.PRESS)
        self.transform = self.local_translate@rotate(self.axis, self.angle)@self.local_scale

        # call Node's draw method to pursue the hierarchical tree calling
        super().draw(projection, view, model, win=win, **param)
               
        
# -------------- classes for keyframe animation -----------------------------------------
class KeyFrames:
    """ Stores keyframe pairs for any value type with interpolation function"""
    def __init__(self, time_value_pairs, interpolation_function=lerp):
        if isinstance(time_value_pairs, dict):  # convert to list of pairs
            time_value_pairs = time_value_pairs.items()
        keyframes = sorted(((key[0], key[1]) for key in time_value_pairs))
        self.times, self.values = zip(*keyframes)  # pairs list -> 2 lists
        self.interpolate = interpolation_function

    def value(self, time):
        """ Computes interpolated value from keyframes, for a given time """

        if (time>=self.times[len(self.times)-1]): 
            return self.values[len(self.times)-1]
        elif (time<=self.times[0]): 
            return self.values[0]
        else:
            index = bisect.bisect_left(self.times, time)
            frac = (time-self.times[index-1])/(self.times[index]-self.times[index-1])
            return self.interpolate(self.values[index-1],self.values[index],frac)
            
    def qvalue(self, time):
        """ Call only for quaternions """

        if (time>=self.times[len(self.times)-1]): 
            return self.values[len(self.times)-1]
        elif (time<=self.times[0]): 
            return self.values[0]
        else:
            index = bisect.bisect_left(self.times, time)
            frac = (time-self.times[index-1])/(self.times[index]-self.times[index-1])
            return quaternion_slerp(self.values[index-1],self.values[index],frac)
            
class TransformKeyFrames:
    """ KeyFrames-like object dedicated to 3D transforms """
    def __init__(self, translate_keys, rotate_keys, scale_keys):
        translate_keysx={}
        translate_keysy={}
        translate_keysz={}
        for k, v in translate_keys.items():
            translate_keysx.update({k: v[0]})
            translate_keysy.update({k: v[1]})
            translate_keysz.update({k: v[2]})
        self.tFramex = KeyFrames(translate_keysx)
        self.tFramey = KeyFrames(translate_keysy)
        self.tFramez = KeyFrames(translate_keysz)
        self.rFrame = KeyFrames(rotate_keys)
        scale_keysx={}
        scale_keysy={}
        scale_keysz={}
        for k, v in scale_keys.items():
            scale_keysx.update({k: v[0]})
            scale_keysy.update({k: v[1]})
            scale_keysz.update({k: v[2]})
        self.sFramex = KeyFrames(scale_keysx)
        self.sFramey = KeyFrames(scale_keysy)
        self.sFramez = KeyFrames(scale_keysz)
   

    def value(self, time):
        """ Compute each component's interpolation and compose TRS matrix """
        tr = translate(vec(self.tFramex.value(time),self.tFramey.value(time),self.tFramez.value(time)))
        rt = quaternion_matrix(self.rFrame.qvalue(time))
        sc = scale(vec(self.sFramex.value(time),self.sFramey.value(time),self.sFramez.value(time)))
        return tr@rt@sc


class KeyFrameControlNode(Node):
    """ Place node with transform keys above a controlled subtree """
    def __init__(self, trans_keys, rotat_keys, scale_keys, **kwargs):
        super().__init__(**kwargs)
        self.keyframes = TransformKeyFrames(trans_keys, rotat_keys, scale_keys)

    def draw(self, projection, view, model, **param):
        """ When redraw requested, interpolate our node transform from keys """
        self.transform = self.keyframes.value(glfw.get_time())
        super().draw(projection, view, model, **param)

        
        
# -------------- classes an functions for skinning animation -----------------------------------------

class SkinnedMesh:
    def __init__(self,attributes,bone_nodes,bone_offsets,begintime,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.,index=None):

        self.vertex_array = VertexArray(attributes, index)
        self.skinning_shader = Shader(SKINNING_VERT, COLOR_FRAG)
        self.bone_nodes = bone_nodes
        self.bone_offsets = bone_offsets
        self.inittransform = inittransform
        self.K_d= K_d
        self.K_a= K_a
        self.K_s= K_s
        self.s= s
        self.begintime= begintime

    def draw(self, projection, view, _model, **_kwargs):
        shid = self.skinning_shader.glid
        GL.glUseProgram(shid)
        names = ['view', 'projection','K_d','K_a','K_s','s','time','mode']
        loc = {n: GL.glGetUniformLocation(shid, n) for n in names}
        
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniform3fv(loc['K_d'], 1, self.K_d)
        GL.glUniform3fv(loc['K_a'], 1, self.K_a)
        GL.glUniform3fv(loc['K_s'], 1, self.K_s)
        GL.glUniform1fv(loc['s'], 1, self.s)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        GL.glUniform1iv(loc['mode'], 1, 1)
        
        # bone world transform matrices need to be passed for skinning
        for bone_id, node in enumerate(self.bone_nodes):
            bone_matrix = self.inittransform @ node.world_transform @ self.bone_offsets[bone_id]
            bone_loc = GL.glGetUniformLocation(shid, 'boneMatrix[%d]' % bone_id)
            GL.glUniformMatrix4fv(bone_loc, 1, True, bone_matrix)
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glUseProgram(0)

class SkinningControlNode(Node):
    """ Place node with transform keys above a controlled subtree """
    def __init__(self, *keys, **kwargs):
        super().__init__(**kwargs)
        self.keyframes = TransformKeyFrames(*keys) if keys[0] else None
        self.world_transform = identity()

    def draw(self, projection, view, model, **param):
        """ When redraw requested, interpolate our node transform from keys """
        if self.keyframes:  
            self.transform = self.keyframes.value(glfw.get_time())
        self.world_transform = model @ self.transform
        super().draw(projection, view, model, **param)
        
def load_skinned(file,begintime,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
    """load resources from file using pyassimp, return node hierarchy """
    try:
        option = pyassimp.postprocess.aiProcessPreset_TargetRealtime_MaxQuality
        scene = pyassimp.load(file, option)
    except pyassimp.errors.AssimpError:
        print('ERROR: pyassimp unable to load', file)
        return []

    # ----- load animations
    def conv(assimp_keys, ticks_per_second):
        """ Conversion from assimp key struct to our dict representation """
        return {key.time / ticks_per_second: key.value for key in assimp_keys}
    transform_keyframes = {}
    if scene.animations:
        anim = scene.animations[0]
        for channel in anim.channels:
            # for each animation bone, store trs dict with {times: transforms}
            # (pyassimp name storage bug, bytes instead of str => convert it)
            transform_keyframes[channel.nodename.data.decode('utf-8')] = (
                conv(channel.positionkeys, anim.tickspersecond),
                conv(channel.rotationkeys, anim.tickspersecond),
                conv(channel.scalingkeys, anim.tickspersecond)
            )

    # ---- prepare scene graph nodes
    # create SkinningControlNode for each assimp node.
    # node creation needs to happen first as SkinnedMeshes store an array of
    # these nodes that represent their bone transforms
    nodes = {}  # nodes: string name -> node dictionary

    def make_nodes(pyassimp_node):
        """ Recursively builds nodes for our graph, matching pyassimp nodes """
        trs_keyframes = transform_keyframes.get(pyassimp_node.name, (None,))
        node = SkinningControlNode(*trs_keyframes, name=pyassimp_node.name,
                                   transform=pyassimp_node.transformation)
        nodes[pyassimp_node.name] = node, pyassimp_node
        node.add(*(make_nodes(child) for child in pyassimp_node.children))
        return node

    root_node = make_nodes(scene.rootnode)

    # ---- create SkinnedMesh objects
    for mesh in scene.meshes:
        # -- skinned mesh: weights given per bone => convert per vertex for GPU
        # first, populate an array with MAX_BONES entries per vertex
        v_bone = np.array([[(0, 0)]*MAX_BONES] * mesh.vertices.shape[0],
                          dtype=[('weight', 'f4'), ('id', 'u4')])
        for bone_id, bone in enumerate(mesh.bones[:MAX_BONES]):
            for entry in bone.weights:  # weight,id pairs necessary for sorting
                v_bone[entry.vertexid][bone_id] = (entry.weight, bone_id)
        v_bone.sort(order='weight')             # sort rows, high weights last
        v_bone = v_bone[:, -MAX_VERTEX_BONES:]  # limit bone size, keep highest

        # prepare bone lookup array & offset matrix, indexed by bone index (id)
        bone_nodes = [nodes[bone.name][0] for bone in mesh.bones]
        bone_offsets = [bone.offsetmatrix for bone in mesh.bones]

        # initialize skinned mesh and store in pyassimp_mesh for node addition
        mesh.skinned_mesh = SkinnedMesh(
                [mesh.vertices,mesh.normals,v_bone['id'],v_bone['weight']],
                bone_nodes,bone_offsets,begintime,inittransform,K_d,K_a,K_s,s,mesh.faces
        )

    # ------ add each mesh to its intended nodes as indicated by assimp
    for final_node, assimp_node in nodes.values():
        final_node.add(*(_mesh.skinned_mesh for _mesh in assimp_node.meshes))

    nb_triangles = sum((mesh.faces.shape[0] for mesh in scene.meshes))
    print('Loaded', file, '\t(%d meshes, %d faces, %d nodes, %d animations)' %
          (len(scene.meshes), nb_triangles, len(nodes), len(scene.animations)))
    pyassimp.release(scene)
    return [root_node]

# ------------  basic scene meshes------------------------------------------


class ColorMesh:

    def __init__(self,attributes,begintime,index=None):
        self.vertex_array = VertexArray(attributes, index)
        self.begintime=begintime

    def draw(self, projection, view, model, color_shader, **params):

        names = ['view', 'projection', 'model', 'time', 'mode']
        loc = {n: GL.glGetUniformLocation(color_shader.glid, n) for n in names}
        GL.glUseProgram(color_shader.glid)

        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        GL.glUniform1iv(loc['mode'], 1, 0)

        # draw triangle as GL_TRIANGLE vertex array, draw array call
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glUseProgram(0)
        
        
class PhongMesh:

    def __init__(self,attributes,begintime,K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.,index=None,inittransform=identity()):
        self.vertex_array = VertexArray(attributes, index)
        self.inittransform = inittransform
        self.K_d= K_d
        self.K_a= K_a
        self.K_s= K_s
        self.s= s
        self.begintime=begintime

    def draw(self, projection, view, model, color_shader=None, **params):

        names = ['view', 'projection', 'model','K_d','K_a','K_s','s','time','mode']
        loc = {n: GL.glGetUniformLocation(color_shader.glid, n) for n in names}
        GL.glUseProgram(color_shader.glid)
        
        
        model = model @ self.inittransform  
        #print(projection@view@model)
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform3fv(loc['K_d'], 1, self.K_d)
        GL.glUniform3fv(loc['K_a'], 1, self.K_a)
        GL.glUniform3fv(loc['K_s'], 1, self.K_s)
        GL.glUniform1fv(loc['s'], 1, self.s)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        GL.glUniform1iv(loc['mode'], 1, 1)
        
        # draw triangle as GL_TRIANGLE vertex array, draw array call
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glUseProgram(0)
        
                        
class TexturedMesh:

    def __init__(self,text,attributes,begintime,K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.,index=None,inittransform=identity()):
        self.shader = Shader(COLOR_VERT, COLOR_FRAG)
        #self.shader = Shader(TEXTURE_VERT, TEXTURE_FRAG)
        self.vertex_array = VertexArray(attributes, index)
        self.wrap = cycle([GL.GL_REPEAT, GL.GL_MIRRORED_REPEAT,
                           GL.GL_CLAMP_TO_BORDER, GL.GL_CLAMP_TO_EDGE])
        self.filter = cycle([(GL.GL_NEAREST, GL.GL_NEAREST),
                             (GL.GL_LINEAR, GL.GL_LINEAR),
                             (GL.GL_LINEAR, GL.GL_LINEAR_MIPMAP_LINEAR)])
        self.wrap_mode, self.filter_mode = next(self.wrap), next(self.filter) 
        self.file=text
        self.texture=Texture(text,self.wrap_mode, *self.filter_mode)
        self.inittransform = inittransform
        self.K_d= K_d
        self.K_a= K_a
        self.K_s= K_s
        self.s= s
        self.begintime=begintime
            

    def draw(self, projection, view, model, win=None, **_kwargs):

        #don't use for the scene
        '''if glfw.get_key(win, glfw.KEY_F6) == glfw.PRESS:
            self.wrap_mode = next(self.wrap)
            self.texture=Texture(self.file,self.wrap_mode, *self.filter_mode)'''

        if glfw.get_key(win, glfw.KEY_F7) == glfw.PRESS:
            self.filter_mode = next(self.filter)
            self.texture=Texture(self.file,self.wrap_mode, *self.filter_mode)

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        names = ['view', 'projection', 'model','K_d','K_a','K_s','s','time','mode']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        
        model = model @ self.inittransform  
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform3fv(loc['K_d'], 1, self.K_d)
        GL.glUniform3fv(loc['K_a'], 1, self.K_a)
        GL.glUniform3fv(loc['K_s'], 1, self.K_s)
        GL.glUniform1fv(loc['s'], 1, self.s)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        GL.glUniform1iv(loc['mode'], 1, 2)

        # texture access setups
        loc = GL.glGetUniformLocation(self.shader.glid, 'diffuseMap')
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(loc, 0)
        self.vertex_array.draw(GL.GL_TRIANGLES)

        # leave clean state for easier debugging
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)
        
        
class PBRMesh:

    def __init__(self,attributes,begintime,albedo=(0.01, 0.01, 0.01),f0=(1.00, 0.71, 0.29),roughness=0.1,index=None,inittransform=identity()):
        self.shader = Shader(PBR_VERT, PBR_FRAG)
        self.vertex_array = VertexArray(attributes, index)
        self.inittransform = inittransform
        self.albedo=albedo
        self.f0=f0
        self.roughness=roughness
        self.begintime=begintime

    def draw(self, projection, view, model, **params):
        
        shid = self.shader.glid
        GL.glUseProgram(shid)
        names = ['view', 'projection', 'model','albedo','f0','roughness','time']
        loc = {n: GL.glGetUniformLocation(shid, n) for n in names}
           
        model = model @ self.inittransform  
        #print(projection@view@model)
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform3fv(loc['albedo'], 1, self.albedo)
        GL.glUniform3fv(loc['f0'], 1, self.f0)
        GL.glUniform1fv(loc['roughness'], 1, self.roughness)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        
        # draw triangle as GL_TRIANGLE vertex array, draw array call
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glUseProgram(0)

class NonFlatGround:
    
    def __init__(self,files,begintime,nbiter=20,wrap_mode=[GL.GL_CLAMP_TO_EDGE,GL.GL_REPEAT],min_filter=[GL.GL_LINEAR,GL.GL_LINEAR],
                                   mag_filter=[GL.GL_LINEAR,GL.GL_LINEAR],inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
        
        self.shader = Shader(COLOR_VERT, COLOR_FRAG)
        self.inittransform = inittransform
        self.K_d= K_d
        self.K_a= K_a
        self.K_s= K_s
        self.s= s
        self.begintime= begintime
        
        tex = np.array(Image.open("heightmap.png"))

        nbiter=tex.shape[0]-1
        
        minc=-200
        maxc=200
        step=(maxc-minc)/nbiter
        xcords=[]
        ycords=[]
        zcords=[]
        for i in range(nbiter+1):
            for j in range(nbiter+1):
                xcords.append(minc+i*step)
                ycords.append(minc+j*step)
                #can switch to random generation of heights
                #zcords.append(random.uniform(0,1))
                zcords.append(tex[i][j]/5.)
        xcord=np.reshape(np.array(xcords),(nbiter+1,nbiter+1))
        ycord=np.reshape(np.array(ycords),(nbiter+1,nbiter+1))
        zcord=np.reshape(np.array(zcords),(nbiter+1,nbiter+1))
            
        position=[]
        normal=[]
        faces=[]
        index=0
        texcoord=[]
        for i in range(nbiter):
            for j in range(nbiter):
                position.append([xcord[i,j],ycord[i,j],zcord[i,j]])
                texcoord.append([j/nbiter,1- i/nbiter])
                position.append([xcord[i,j+1],ycord[i,j+1],zcord[i,j+1]])
                texcoord.append([(j+1)/nbiter,1 - i/nbiter])
                position.append([xcord[i+1,j+1],ycord[i+1,j+1],zcord[i+1,j+1]])
                texcoord.append([(j+1)/nbiter,1 - (i+1)/nbiter])
                faces.append([index,index+1,index+2])
                index=index+3
                xnorm=(ycord[i,j+1]-ycord[i,j])*(zcord[i+1,j+1]-zcord[i,j])-(ycord[i+1,j+1]-ycord[i,j])*(zcord[i,j+1]-zcord[i,j])
                ynorm=(zcord[i,j+1]-zcord[i,j])*(xcord[i+1,j+1]-xcord[i,j])-(zcord[i+1,j+1]-zcord[i,j])*(xcord[i,j+1]-xcord[i,j])
                znorm=(xcord[i,j+1]-xcord[i,j])*(ycord[i+1,j+1]-ycord[i,j])-(xcord[i+1,j+1]-xcord[i,j])*(ycord[i,j+1]-ycord[i,j])
                normstartn=normalized(vec(-xnorm,-ynorm,-znorm))
                for k in range(3):
                    normal.append([normstartn[0],normstartn[1],normstartn[2]])
                position.append([xcord[i,j],ycord[i,j],zcord[i,j]])
                texcoord.append([j/nbiter,1 - i/nbiter])
                position.append([xcord[i+1,j+1],ycord[i+1,j+1],zcord[i+1,j+1]])
                texcoord.append([(j+1)/nbiter,1 - (i+1)/nbiter])
                position.append([xcord[i+1,j],ycord[i+1,j],zcord[i+1,j]])
                texcoord.append([(j)/nbiter,1 - (i+1)/nbiter])
                faces.append([index,index+1,index+2])
                index=index+3
                xnorm=(ycord[i+1,j+1]-ycord[i,j])*(zcord[i+1,j]-zcord[i,j])-(ycord[i+1,j]-ycord[i,j])*(zcord[i+1,j+1]-zcord[i,j])
                ynorm=(zcord[i+1,j+1]-zcord[i,j])*(xcord[i+1,j]-xcord[i,j])-(zcord[i+1,j]-zcord[i,j])*(xcord[i+1,j+1]-xcord[i,j])
                znorm=(xcord[i+1,j+1]-xcord[i,j])*(ycord[i+1,j]-ycord[i,j])-(xcord[i+1,j]-xcord[i,j])*(ycord[i+1,j+1]-ycord[i,j])
                normstartn=normalized(vec(-xnorm,-ynorm,-znorm))
                for k in range(3):
                    normal.append([normstartn[0],normstartn[1],normstartn[2]])

        
        
        self.files=files
        #self.pos=np.array(position)
        self.vertex_array = VertexArray([position,normal,texcoord],faces)
        self.multtext=MultiTexture(self.files,wrap_mode=wrap_mode,min_filter=min_filter,mag_filter=mag_filter)
         
    def draw(self, projection, view, model, win=None, **_kwargs):

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        names = ['view', 'projection', 'model','K_d','K_a','K_s','s','time','mode']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        
        model = model @ self.inittransform  
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform3fv(loc['K_d'], 1, self.K_d)
        GL.glUniform3fv(loc['K_a'], 1, self.K_a)
        GL.glUniform3fv(loc['K_s'], 1, self.K_s)
        GL.glUniform1fv(loc['s'], 1, self.s)
        GL.glUniform1fv(loc['time'], 1, time.time()-self.begintime)
        GL.glUniform1iv(loc['mode'], 1, 3)
        # texture access setups
        for i in range(len(self.files)):
            temp = 'diffuseMap'+str(i+1)
            loc = GL.glGetUniformLocation(self.shader.glid,temp)
            GL.glActiveTexture(GL.GL_TEXTURE0+i)
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.multtext.glid[i])
            GL.glUniform1i(loc, i)              
        self.vertex_array.draw(GL.GL_TRIANGLES)

        # leave clean state for easier debugging
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)




# ------------  simple objects with light for the project------------------------------------------        
        

class Pyramid(PhongMesh):

    def __init__(self,begintime,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
        
        rotate=np.array([[0,1,0],[-1,0,0],[0,0,1]])
        
        normal=[]
        position=[]
        startp=(.5,.5,0)
        
        for i in range(4):
            position.append([startp[0],startp[1],startp[2]])
            position.append([0,0,1])
            startp1=startp
            startp=rotate.dot(startp1)
            position.append([startp[0],startp[1],startp[2]])
            xnorm=(startp1[1]-startp[1])*(1-startp[2])-(0-startp[1])*(startp1[2]-startp[2])
            ynorm=(startp1[2]-startp[2])*(0-startp[0])-(1-startp[2])*(startp1[0]-startp[0])
            znorm=(startp1[0]-startp[0])*(0-startp[1])-(0-startp[0])*(startp1[1]-startp[1])
            normstartn=normalized(vec(xnorm,ynorm,znorm))
            for k in range(3):
                normal.append([normstartn[0],normstartn[1],normstartn[2]])
        startp1=rotate.dot(startp)
        startp2=rotate.dot(startp1)
        startp3=rotate.dot(startp2)
        position.append([startp[0],startp[1],startp[2]])
        position.append([startp1[0],startp1[1],startp1[2]])
        position.append([startp2[0],startp2[1],startp2[2]])
        
        position.append([startp[0],startp[1],startp[2]])
        position.append([startp2[0],startp2[1],startp2[2]])
        position.append([startp3[0],startp3[1],startp3[2]])
        for i in range(6):
            normal.append([0,0,-1])
        faces = np.array(([0,1,2], [3,4,5] ,[6,7,8], [9,10,11], [12,13,14], [15,16,17]))
        

        super().__init__([position, normal], begintime, K_d,  K_a, K_s, s,faces, inittransform)
        
              
        
class Cone(PhongMesh):

    def __init__(self,begintime,nbt=20,radius=.5,height=1,origin=(0,0,0),inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):

        position=[]
        normal=[]
        faces=[]

        alpha=2*np.pi/nbt
        index=0 
        
        for i in range(nbt):
            startp1=(radius*np.cos(i*alpha)+origin[0],radius*np.sin(i*alpha)+origin[1],origin[2])
            position.append([startp1[0],startp1[1],startp1[2]])
            faces.append([index,index+1,index+2])
            index=index+3
            startp2=(radius*np.cos((i+1)*alpha)+origin[0],radius*np.sin((i+1)*alpha)+origin[1],origin[2])
            position.append([startp2[0],startp2[1],startp2[2]])
            faces.append([index,index+1,index+2])
            index=index+3
            position.append([0,0,1])
            faces.append([index,index+1,index+2])
            index=index+3
            xnorm=(startp2[1]-startp1[1])*(1-startp1[2])-(0-startp1[1])*(startp2[2]-startp1[2])
            ynorm=(startp2[2]-startp1[2])*(0-startp1[0])-(1-startp1[2])*(startp2[0]-startp1[0])
            znorm=(startp2[0]-startp1[0])*(0-startp1[1])-(0-startp1[0])*(startp2[1]-startp1[1])
            normstartn=normalized(vec(xnorm,ynorm,znorm))
            for k in range(3):
                normal.append([normstartn[0],normstartn[1],normstartn[2]])
                

            
        for i in range(nbt):
            startp=(radius*np.cos(i*alpha)+origin[0],radius*np.sin(i*alpha)+origin[1],origin[2])
            position.append([startp[0],startp[1],startp[2]])
            normal.append([0,0,-1])
            faces.append([index,index+1,index+2])
            index=index+3
            startp=(radius*np.cos((i+1)*alpha)+origin[0],radius*np.sin((i+1)*alpha)+origin[1],origin[2])
            position.append([startp[0],startp[1],startp[2]])
            normal.append([0,0,-1])
            faces.append([index,index+1,index+2])
            index=index+3
            position.append([0,0,0])
            normal.append([0,0,-1])
            faces.append([index,index+1,index+2])
            index=index+3

        super().__init__([position, normal],begintime,K_d,K_a,K_s,s,faces,inittransform)  
        
        
        
# ------------  downloaded node-objects ------------------------------------------      
    

class Cube(Node):
    """ Cube """
    def __init__(self,begintime,mode,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
        super().__init__()
        self.add(*load('cube.obj',begintime,mode,inittransform,K_d=K_d,K_a=K_a,K_s=K_s,s=s))   

class Cylinder(Node):
    """ Cylinder """
    def __init__(self,begintime,mode,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
        super().__init__()
        self.add(*load('cylinder.obj',begintime,mode,inittransform,K_d=K_d,K_a=K_a,K_s=K_s,s=s))   
class Dino_head(Node):
    """ Part 1 of dino """
    def __init__(self,begintime,inittransform=identity(),albedo=(0.01, 0.01, 0.01),f0=(1.00, 0.71, 0.29),roughness=0.1):
        super().__init__()
        self.add(*loadpbr('head.obj',begintime,inittransform,albedo,f0,roughness))        
class Dino_body(Node):
    """ Part 2 of dino """
    def __init__(self,begintime,inittransform=identity(),albedo=(0.01, 0.01, 0.01),f0=(1.00, 0.71, 0.29),roughness=0.1):
        super().__init__()
        self.add(*loadpbr('body.obj',begintime,inittransform,albedo,f0,roughness))         
class Volcano(Node):
    """ Volcano """
    def __init__(self,begintime,inittransform=identity(),albedo=(0.01, 0.01, 0.01),f0=(1.00, 0.71, 0.29),roughness=0.1):
        super().__init__()
        self.add(*loadpbr('volcano.obj',begintime,inittransform,albedo,f0,roughness)) 

        
        
# ------------  skybox classes ---------------------------------------------------------------------------       
        
class Skybox:
    def __init__(self):
        self.shader = Shader(SKY_VERT, SKY_FRAG)
        position =10*np.array((     
        (-1.0,  1.0, -1.0),
        (-1.0, -1.0, -1.0),
        ( 1.0, -1.0, -1.0),
        ( 1.0, -1.0, -1.0),
        ( 1.0,  1.0, -1.0),
        (-1.0,  1.0, -1.0),

        (-1.0, -1.0,  1.0),
        (-1.0, -1.0, -1.0),
        (-1.0,  1.0, -1.0),
        (-1.0,  1.0, -1.0),
        (-1.0,  1.0,  1.0),
        (-1.0, -1.0,  1.0),

        (1.0, -1.0, -1.0),
        (1.0, -1.0,  1.0),
        (1.0,  1.0,  1.0),
        (1.0,  1.0,  1.0),
        (1.0,  1.0, -1.0),
        (1.0, -1.0, -1.0),

        (-1.0, -1.0,  1.0),
        (-1.0,  1.0,  1.0),
        (1.0,  1.0,  1.0),
        (1.0,  1.0,  1.0),
        (1.0, -1.0,  1.0),
        (-1.0, -1.0,  1.0),

        (-1.0,  1.0, -1.0),
        (1.0,  1.0, -1.0),
        (1.0,  1.0,  1.0),
        (1.0,  1.0,  1.0),
        (-1.0,  1.0,  1.0),
        (-1.0,  1.0, -1.0),

        (-1.0, -1.0, -1.0),
        (-1.0, -1.0,  1.0),
        (1.0, -1.0, -1.0),
        (1.0, -1.0, -1.0),
        (-1.0, -1.0,  1.0),
        (1.0, -1.0,  1.0)), np.float32)
        attributes=[position]
        self.vertex_array = VertexArray(attributes,usage=GL.GL_STATIC_DRAW)
        #faces=["skybox_right.png","skybox_left.png","skybox_top.png","skybox_bottom.png","skybox_front.png","skybox_back.png"]
        faces=["purplenebula_rt.png","purplenebula_lf.png","purplenebula_up.png","purplenebula_dn.png","purplenebula_ft.png","purplenebula_bk.png"]
        self.texture=CubeTexture(faces)

         
    def draw(self, projection, view, model, win=None, **_kwargs):

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        names = ['view', 'projection', 'model']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)

        # texture access setups
        GL.glDepthFunc(GL.GL_LEQUAL)
        loc = GL.glGetUniformLocation(self.shader.glid, 'skybox')
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, self.texture.glid)
        GL.glUniform1i(loc, 0)
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0)
        GL.glDepthFunc(GL.GL_LESS)
        GL.glUseProgram(0)
        
# ------------  classes for physical animation -----------------------------------------------------------------------         
        
class Particles:

    def __init__(self, text, initposition=(1,0,0), initvelocity=(0.25,0.5,0.25), sizecoef=1):
        self.shader = Shader(PART_VERT, PART_FRAG)
        in_offset= np.array(((-1,-1,0),(1,-1,0),(-1,1,0),(1,1,0)), np.float32)
        pindex= np.array((0,3,2,0,1,3), np.uint32)  
        in_position= np.array((initposition,initposition,initposition,initposition),np.float32)
        in_velocity= np.array((initvelocity,initvelocity,initvelocity,initvelocity),np.float32) 
        in_part_start_time=np.transpose([(1,1,1,1)])   
        in_part_size=sizecoef*np.transpose([(0.5,0.5,0.5,0.5)])
        poffset=in_offset
        position=in_position
        p_index=pindex
        pvelocity=in_velocity
        ppart_start_time=in_part_start_time
        ppart_size=in_part_size

        #position
        nbt=150
        for i in range(nbt):
            poffset=np.append(poffset,in_offset,axis=0)
            p_index=np.append(p_index,pindex+4 *(i+1))
            pv=initposition+np.random.rand(1,3)
            randpos=np.concatenate((pv,pv,pv,pv))
            position=np.append(position,randpos,axis=0)
            pv=np.random.rand(1,3)
            randvel=initvelocity+np.concatenate((pv,pv,pv,pv))
            pvelocity=np.append(pvelocity,randvel,axis=0)
            p=round(random.uniform(0, 2),2)
            ppart_start_time =np.append(ppart_start_time,np.transpose([(p,p,p,p)]),axis=0)
            p=sizecoef*round(random.uniform(0, 1),2)
            ppart_size =np.append(ppart_size,np.transpose([(p,p,p,p)]),axis=0)
            
        attributes=[poffset,position,pvelocity,ppart_start_time,ppart_size]
        self.vertex_array = VertexArray(attributes, index=p_index,usage=GL.GL_DYNAMIC_DRAW)

        self.file=text
        self.texture=Texture(text,wrap_mode=GL.GL_CLAMP_TO_EDGE)
            

    def draw(self, projection, view, model, win=None, **_kwargs):

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        names = ['view', 'projection', 'model','type','global_time','particle_life_time', 'showmode']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        

        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform1iv(loc['type'], 1, 1)
        GL.glUniform1fv(loc['global_time'], 1, glfw.get_time())
        GL.glUniform1fv(loc['particle_life_time'], 1, 2)
        GL.glUniform1iv(loc['showmode'], 1, 1)


        # texture access setups
        loc = GL.glGetUniformLocation(self.shader.glid, 'diffuseMap')
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(loc, 0)
        #L.glDisable(GL.GL_DEPTH_TEST)
        GL.glDepthMask(0)  
        GL.glEnable(GL.GL_BLEND);
        GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glDepthMask(1)
        GL.glDisable(GL.GL_BLEND);
        # leave clean state for easier debugging
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)
        
        
        
class Intro:

    def __init__(self, file):
        self.shader = Shader(PART_VERT, PART_FRAG)
        #in camera coordinates
        poffset=np.array(((-1,-1,0),(1,-1,0),(1,1,0),(-1,1,0)), np.float32)
        pindex=np.array(((0, 1, 2), (0, 2, 3)), np.uint32)
        position= np.array(((0,-1.1,0),(0,-1.1,0),(0,-1.1,0),(0,-1.1,0)),np.float32)
        pvelocity= np.array(((0,0.1,0),(0,0.1,0),(0,0.1,0),(0,0.1,0)),np.float32) 
        ppart_start_time=np.transpose([(0,0,0,0)])   
        ppart_size=np.transpose([(2,2,2,2)])
        

        attributes=[poffset,position,pvelocity,ppart_start_time,ppart_size]
        self.vertex_array = VertexArray(attributes, index=pindex, usage=GL.GL_DYNAMIC_DRAW)
        self.file=file
        self.texture=Texture(file,wrap_mode=GL.GL_CLAMP_TO_EDGE)
        self.showmode=1
            



    def draw(self, projection, view, model, win=None, **_kwargs):

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        names = ['view', 'projection', 'model','type','global_time','particle_life_time','showmode']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        
        if glfw.get_key(win, glfw.KEY_F8) == glfw.PRESS:
            if self.showmode==1:
                self.showmode=0
            else:
                self.showmode=1

        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)
        GL.glUniform1iv(loc['type'], 1, 2)
        GL.glUniform1fv(loc['global_time'], 1, glfw.get_time())
        GL.glUniform1fv(loc['particle_life_time'], 1, 21)
        GL.glUniform1iv(loc['showmode'], 1, self.showmode)


        # texture access setups
        loc = GL.glGetUniformLocation(self.shader.glid, 'diffuseMap')
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(loc, 0)
        #L.glDisable(GL.GL_DEPTH_TEST)
        #GL.glDepthMask(0)  
        #GL.glEnable(GL.GL_BLEND);
        #GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
        self.vertex_array.draw(GL.GL_TRIANGLES)
        #GL.glDepthMask(1)
        #GL.glDisable(GL.GL_BLEND);
        # leave clean state for easier debugging
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glUseProgram(0)
        
# ------------  class for planet with refraction -------------------------------------------------------------    
      
class Planet:

    def __init__(self, file, inittransform=identity()):
        self.shader = Shader(PLANET_VERT, PLANET_FRAG)
        try:
            option = pyassimp.postprocess.aiProcessPreset_TargetRealtime_MaxQuality
            scene = pyassimp.load(file, option)
        except pyassimp.errors.AssimpError:
            print('ERROR: pyassimp unable to load', file)

        
        for mat in scene.materials:
            mat.tokens = dict(reversed(list(mat.properties.items())))
        
        # prepare mesh nodes
        meshes = []
        for mesh in scene.meshes:
            mat = scene.materials[mesh.materialindex].tokens
            attributes=[mesh.vertices,mesh.normals]
            
        self.vertex_array = VertexArray(attributes, index=mesh.faces)    
        faces=["purplenebula_rt.png","purplenebula_lf.png","purplenebula_up.png","purplenebula_dn.png","purplenebula_ft.png","purplenebula_bk.png"]
        self.texture=CubeTexture(faces)
        self.inittransform=inittransform
         
    def draw(self, projection, view, model, win=None, **_kwargs):

        GL.glUseProgram(self.shader.glid)

        # projection geometry 
        model = model @ self.inittransform  
        names = ['view', 'projection', 'model']
        loc = {n: GL.glGetUniformLocation(self.shader.glid, n) for n in names}
        GL.glUseProgram(self.shader.glid)
        GL.glUniformMatrix4fv(loc['view'], 1, True, view)
        GL.glUniformMatrix4fv(loc['projection'], 1, True, projection)
        GL.glUniformMatrix4fv(loc['model'], 1, True, model)

        # texture access setups
        #GL.glDepthFunc(GL.GL_LEQUAL)
        loc = GL.glGetUniformLocation(self.shader.glid, 'skybox')
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, self.texture.glid)
        GL.glUniform1i(loc, 0)
        
        self.vertex_array.draw(GL.GL_TRIANGLES)
        GL.glBindTexture(GL.GL_TEXTURE_CUBE_MAP, 0)
        #GL.glDepthFunc(GL.GL_LESS)
        GL.glUseProgram(0)

# -------------- 3D resource loader -----------------------------------------------------------------------
def load(file,begintime,mode=0,inittransform=identity(),K_d=(1,1,1),K_a=(0,0,0),K_s=(1,1,1),s=16.):
    """ load resources from file using pyassimp"""
    try:
        option = pyassimp.postprocess.aiProcessPreset_TargetRealtime_MaxQuality
        scene = pyassimp.load(file, option)
    except pyassimp.errors.AssimpError:
        print('ERROR: pyassimp unable to load', file)
        return []  # error reading => return empty list

    # mode for normals without light for simple objects
    if (mode==0):
        meshes = [ColorMesh([m.vertices, m.normals],begintime,m.faces) for m in scene.meshes]
        
    # mode for Phong shading with light
    if (mode==1):
        for mat in scene.materials:
            mat.tokens = dict(reversed(list(mat.properties.items())))
            
        # prepare mesh nodes
        meshes = []
        for mesh in scene.meshes:
            mat = scene.materials[mesh.materialindex].tokens
            node = Node(transform=inittransform, K_d=mat.get('diffuse', K_d),
                    K_a=mat.get('ambient', K_a),
                    K_s=mat.get('specular', K_s),
                    s=mat.get('shininess', s)) 
            node.add(PhongMesh([mesh.vertices,mesh.normals],begintime,K_d,K_a,K_s,s,mesh.faces))
            meshes.append(node)
            
    # mode for texture+shading+light        
    if (mode==2):
            path = os.path.dirname(file)
            path = os.path.join('.', '') if path == '' else path
            for mat in scene.materials:
                mat.tokens = dict(reversed(list(mat.properties.items())))
            if 'file' in mat.tokens:  # texture file token
                tname = mat.tokens['file'].split('/')[-1].split('\\')[-1]
                # search texture in file's whole subdir since path often screwed up
                tname = [os.path.join(d[0], f) for d in os.walk(path) for f in d[2] 
                         if tname.startswith(f) or f.startswith(tname)]
                if tname:
                    mat.texture = tname[0]
                else:
                    print('Failed to find texture:', tname)
            meshes = []
            for mesh in scene.meshes:
                mat = scene.materials[mesh.materialindex].tokens
                node = Node(transform=inittransform,K_d=mat.get('diffuse', K_d),
                K_a=mat.get('ambient', K_a),
                K_s=mat.get('specular', K_s),
                s=mat.get('shininess', s))
                texture = scene.materials[mesh.materialindex].texture
            
            # tex coords in raster order: compute 1 - y to follow OpenGL convention
                tex_uv = ((0, 1) + mesh.texturecoords[0][:, :2] * (1, -1)
                      if mesh.texturecoords.size else None)
            
            # create the textured mesh object from texture, attributes, and indices
                node.add(TexturedMesh(texture, [mesh.vertices,mesh.normals,tex_uv],begintime,K_d,K_a,K_s,s,mesh.faces))
                meshes.append(node)

    size = sum((mesh.faces.shape[0] for mesh in scene.meshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(scene.meshes), size))

    pyassimp.release(scene)
    return meshes
    
    
    
def loadpbr(file,begintime,inittransform=identity(),albedo=(0.01, 0.01, 0.01),f0=(1.00, 0.71, 0.29),roughness=0.1):
    """ load resources from file using pyassimp"""
    try:
        option = pyassimp.postprocess.aiProcessPreset_TargetRealtime_MaxQuality
        scene = pyassimp.load(file, option)
    except pyassimp.errors.AssimpError:
        print('ERROR: pyassimp unable to load', file)
        return []  # error reading => return empty list
        
    for mat in scene.materials:
        mat.tokens = dict(reversed(list(mat.properties.items())))
    meshes = []
    for mesh in scene.meshes:
        mat = scene.materials[mesh.materialindex].tokens
        node = Node(transform=inittransform,albedo=albedo,f0=f0,roughness=roughness) 
        node.add(PBRMesh([mesh.vertices,mesh.normals],begintime,albedo,f0,roughness,mesh.faces))
        meshes.append(node)
    size = sum((mesh.faces.shape[0] for mesh in scene.meshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(scene.meshes), size))
    pyassimp.release(scene)
    return meshes


# ------------  viewer class & window management --------------------------------------------
class GLFWTrackball(Trackball):
    """ Use in Viewer for interactive viewpoint control """

    def __init__(self, win):
        """ Init needs a GLFW window handler 'win' to register callbacks """
        super().__init__()
        self.mouse = (0, 0)
        glfw.set_cursor_pos_callback(win, self.on_mouse_move)
        glfw.set_scroll_callback(win, self.on_scroll)

    def on_mouse_move(self, win, xpos, ypos):
        """ Rotate on left-click & drag, pan on right-click & drag """
        old = self.mouse
        self.mouse = (xpos, glfw.get_window_size(win)[1] - ypos)
        if glfw.get_mouse_button(win, glfw.MOUSE_BUTTON_LEFT):
            self.drag(old, self.mouse, glfw.get_window_size(win))
        if glfw.get_mouse_button(win, glfw.MOUSE_BUTTON_RIGHT):
            self.pan(old, self.mouse)

    def on_scroll(self, win, _deltax, deltay):
        """ Scroll controls the camera distance to trackball center """
        self.zoom(deltay, glfw.get_window_size(win)[1])


class Viewer:
    """ GLFW viewer window, with classic initialization & graphics loop """

    def __init__(self, width=640, height=480):

        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL.GL_TRUE)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.RESIZABLE, False)
        self.win = glfw.create_window(width, height, 'Viewer', None, None)
        self.trackball = GLFWTrackball(self.win) #added for camera rotation
        glfw.make_context_current(self.win)

        # register event handlers
        glfw.set_key_callback(self.win, self.on_key)

        # useful message to check OpenGL renderer characteristics
        print('OpenGL', GL.glGetString(GL.GL_VERSION).decode() + ', GLSL',
              GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION).decode() +
              ', Renderer', GL.glGetString(GL.GL_RENDERER).decode())

        # initialize GL by setting viewport and default render characteristics
        GL.glClearColor(0.1, 0.1, 0.1, 0.1)
        GL.glEnable(GL.GL_DEPTH_TEST)       

        # compile and initialize shader programs once globally
        self.color_shader = Shader(COLOR_VERT, COLOR_FRAG)

        # initially empty list of object to draw
        self.drawables = []
        
    def run(self):
        """ Main render loop for this OpenGL window """
        while not glfw.window_should_close(self.win):
            GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

            winsize = glfw.get_window_size(self.win)
            view = self.trackball.view_matrix()
            projection = self.trackball.projection_matrix(winsize)
            for drawable in self.drawables:
                drawable.draw(projection, view, identity(),
                              color_shader=self.color_shader, win=self.win)
            glfw.swap_buffers(self.win)
            glfw.poll_events()

    def add(self, *drawables):
        """ add objects to draw in this window """
        self.drawables.extend(drawables)

    def on_key(self, _win, key, _scancode, action, _mods):
        """ 'Q' or 'Escape' quits """
        if action == glfw.PRESS or action == glfw.REPEAT:
            if key == glfw.KEY_ESCAPE or key == glfw.KEY_Q:
                glfw.set_window_should_close(self.win, True)
            if key == glfw.KEY_W:
                GL.glPolygonMode(GL.GL_FRONT_AND_BACK, next(self.fill_modes))
            if key == glfw.KEY_SPACE:
                glfw.set_time(0)


# -------------- main program and scene setup ---------------------------------------
def main():
    """ create a window, add scene objects, then run rendering loop """
    viewer = Viewer()
    begintime = time.time()
    

    # skybox
    viewer.add(Skybox())
    
    # squared plane of the size 400*400
    viewer.add(NonFlatGround(["texture.png","grassMIT.png","grassMIT.png","grassMIT.png"],begintime=begintime,wrap_mode=[GL.GL_CLAMP_TO_EDGE,GL.GL_REPEAT,GL.GL_REPEAT,GL.GL_REPEAT],
                             min_filter=[GL.GL_LINEAR,GL.GL_LINEAR,GL.GL_LINEAR,GL.GL_LINEAR],mag_filter=[GL.GL_LINEAR,GL.GL_LINEAR,GL.GL_LINEAR,GL.GL_LINEAR],K_d=(1,1,1),K_a=(0.5,0.5,0.5)))

    # text for introduction
    viewer.add(Intro("image6.png"))
    
    # dino with pbr, hierarchy and key control
    head = Dino_head(begintime=begintime, albedo=(0.47,0.78,0.73), f0=(0.17, 0.17, 0.17), roughness=0.5)
    body = Dino_body(begintime=begintime, albedo=(0.47,0.78,0.73), f0=(0.17, 0.17, 0.17), roughness=0.5)
    cylinder = Cylinder(begintime=begintime,mode=1, K_d=(0.01,0.1,0.1), K_a=(0.7,0.2,0.5))  
    cylinder_node= Node(name='dinocylinder',transform=translate(0.5,2,4.5)@rotate((1, 0, 0),45)@scale(0.5, 0.5, 0.5))    
    cylinder_node.add(cylinder) 
    head_node= RotationControlNode(glfw.KEY_1, glfw.KEY_2, axis=vec(0, 0, 1), angle=0, translate=translate(0.5,4.5,2))    
    head_node.add(head,cylinder_node)
    base_shape= RotationControlNode(glfw.KEY_3, glfw.KEY_4, axis=vec(1, 0, 0),angle=90, translate=translate(70,-80,16.4), scale=scale(2,2,2))    
    base_shape.add(body,head_node)
    viewer.add(base_shape)

    # spruce with hierarchy
    firstCone=Cone(begintime=begintime,K_d=(0.137255, 0.556863, 0.137255), K_a=(0.2,0.5,0.2), inittransform=translate(-67,-27,36)@scale(8, 8, 4))
    first_shape=Node(name='firstcone')
    first_shape.add(firstCone)
    secondCone=Cone(begintime=begintime, K_d=(0.137255, 0.556863, 0.137255), K_a=(0.2,0.5,0.2), inittransform=translate(-67,-27,32)@scale(12, 12, 4))
    second_shape=Node(name='secondcone', children=[secondCone])
    second_shape.add(first_shape)
    thirdCone=Cone(begintime=begintime, K_d=(0.137255, 0.556863, 0.137255), K_a=(0.2,0.5,0.2), inittransform=translate(-67,-27,26)@scale(16, 16, 8))
    third_shape=Node(name='thirdcone',children=[thirdCone])
    third_shape.add(second_shape)
    cylinder=Cylinder(begintime=begintime,mode=1,K_d=(0.1,0.1,0.1),K_a=(0.7,0.5,0.3),inittransform=translate(-67,-27,23.5)@rotate((1, 0, 0),90)@scale(2,2,2))
    base_node=Node(name='sprucecylinder', children=[cylinder])
    base_node.add(third_shape)
    viewer.add(base_node)
       
    # keyframe animation: flying cube and key control (space)
    flcube= Cube (begintime=begintime, mode=1, K_d=(0.01,0.1,0.1), K_a=(0.7,0.2,0.5))
    translate_keys = {0: vec(117, -96, 45), 5: vec(100, -70, 38), 10: vec(96, -69, 25.1)}
    rotate_keys = {0: quaternion(), 5: quaternion_from_euler(180, 45, 90),
                   10: quaternion()}
    scale_keys = {0: vec(2,2,2), 10: vec(2,2,2)}
    keynode = KeyFrameControlNode(translate_keys, rotate_keys, scale_keys)
    keynode.add(flcube,flcube)
    viewer.add(keynode)
    
    # cube with texture and key control (F7 works for filters)
    tr=translate(96,-69,21.5)@scale(5, 5, 5)
    texturecube = Cube (begintime=begintime, mode=2, inittransform=tr)
    viewer.add(texturecube)
    
    
    # without bunny with texture
    #tr=translate(4.6,0,1.1)@rotate(vec(1,0, 0),90)@scale(2, 2, 2)
    #viewer.add(load("bunny.obj",2, inittransform=tr)[0])

    # camping (pyramides in phong)
    tr=translate(80,-43,18.5)@scale(15, 15, 15)
    viewer.add(Pyramid(begintime=begintime,inittransform=tr, K_d=(0.55,0.09,0.09), K_a=(0.55,0.1,0.1)))
    
    tr=translate(96,-57,19.5)@scale(15, 15, 15)
    viewer.add(Pyramid(begintime=begintime,inittransform=tr, K_d=(0.55,0.09,0.09), K_a=(0.55,0.1,0.1)))
    
    # dino in skinning and key control (space)
    tr=translate(50,-40,17.5)@rotate((1, 0, 0),90)@rotate((0, 1, 0),-90)@scale(7, 7, 7)
    viewer.add(*load_skinned("Dinosaurus_attack.dae", begintime=begintime, K_d=(0.137255,0.556863,0.419608), K_a=(0.3,0.2,0.1), inittransform=tr))
    
    tr=translate(-30,-40,21)@rotate((1, 0, 0),90)@rotate((0, 1, 0),-90)@scale(7, 7, 7)
    viewer.add(*load_skinned("Dinosaurus_idle2.dae", begintime=begintime, K_d=(0.137255,0.556863,0.419608), K_a=(0.3,0.2,0.1), inittransform=tr))
    
    #pbr and physical animation: volcano and smoke
    tr=translate(23,85,43)@rotate((1, 0, 0),90)@scale(60, 60, 60)
    volcano = Volcano (begintime=begintime, albedo=(0.47, 0.78, 0.73), f0=(0.56, 0.57, 0.58), roughness=0.5, inittransform=tr)
    viewer.add(volcano)
    viewer.add(Particles("firesmokeparticle.png",initposition=(18.3,77,72),initvelocity=(48,20,48),sizecoef=40))
    
    
    # physical animation: fire
    viewer.add(Particles("fire.png",initposition=(97,-36,33), initvelocity=(1,2,1), sizecoef=30))
    
    # ice planets with refraction  
    viewer.add(Planet(file="earth.obj",inittransform=translate(-10,-190,100)@scale(0.05, 0.05, 0.05)))
    viewer.add(Planet(file="earth.obj",inittransform=translate(150,-190,140)@scale(0.05, 0.05, 0.05)))

    
    # start rendering loop
    viewer.run()


if __name__ == '__main__':
    glfw.init()                # initialize window system glfw
    main()                     # main function keeps variables locally scoped
    glfw.terminate()           # destroy all glfw windows and GL contexts
